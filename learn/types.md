# Random stuff about types

From the [Q&A after the talk](https://youtu.be/A4AA8nweYAg?t=1908) by Ben Greenman about deep and shallow typing in typed racket 

> (Jay McCarthy) What is your understanding of the word type. How would you define it and do you think that your definition is different than other people in industry think about type and other people in programming theory think about type.

> (Matthias Felleisen) [...] Intellij even interprets Python quasi types and doc strings [...] and acts like python had types and python was the most, you know, it was really untyped. 

> (Matthias Felleisen (in a different context) [...] the opposite view is python or other dirty languages that really don't understand types at all. 
 
From my lay perspective, Python has a very clear understanding of types and is indeed a strongly typed language, but also dynamically typed. Python does not have "quasi types", it has types. Everything in Python is an object and every object has a type (that in the normal case doesn't change, unless somebody fiddels with Python internals). Where is the missing understanding of types?

Also what's a dirty langauge? What is a clean language?
