# Links

* something like type(obj) for racket: [describe](https://stackoverflow.com/questions/11566886/how-do-i-get-the-type-of-a-value-in-scheme/11567289)
* [evaluation model](https://docs.racket-lang.org/reference/eval-model.html)
* (for comparison) [common lisp](http://www.lispworks.com/documentation/HyperSpec/Body/03_a.htm)
* (for comparison) [scheme](https://scheme.com/tspl4/start.html#./start:h3)

# Nice exercises

* recursion: implement fak, map and (TODO flatten) - see learn-recursion.rkt
* graphics: big-bang, 2htdp - see graphics.rkt
* types: see types.rkt

# Random thoughts about racket vs "normal languages (Python? C?)"

## Everything is an expression (at runtime)

Maybe one of the most important differences. Python has quite a few statements that are not nestable inside expressions and lambda in Python is a function definition as expression, but is limited to contain only expressions.

(If it's not an expression then it is a macro or syntax and that is compile time)

## There is only one way to define things

define always creates an object, it might be a value or a function. This is determined by the syntax.

macros syntax
